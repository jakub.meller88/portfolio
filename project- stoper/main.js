const start = document.querySelector("button.main");
const reset = document.querySelector("button.reset");
const div = document.querySelector("div div");

let time = 0;
let active = false;
let indexInterval;

const timer = () => {
  if (!active) {
    active = !active;
    start.textContent = "Pause";
    indexInterval = setInterval(timeStart, 10);
  } else {
    active = !active;
    start.textContent = "Start";
    clearInterval(indexInterval);
  }
};
const timeStart = () => {
  time++;
  div.textContent = (time / 100).toFixed(2);
};
const timerReset = () => {
  div.textContent = "---";
  time = 0;
  active = false;
  start.textContent = "Start";
  clearInterval(indexInterval);
};

start.addEventListener("click", timer);
reset.addEventListener("click", timerReset);
