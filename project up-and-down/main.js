const upButton = document.querySelector(".firstButton");
const downButton = document.querySelector(".secondButton");

let red = 100;
let green = 100;
let blue = 100;
let red1 = 100;
let green1 = 100;
let blue1 = 100;
let red2 = 100;
let green2 = 100;
let blue2 = 100;

document.body.style.backgroundColor = `rgb(${red}, ${green}, ${blue})`;
upButton.style.color = `rgb(${red1}, ${green1}, ${blue1})`;
downButton.style.color = `rgb(${red2}, ${green2}, ${blue2})`;

const changeColor = e => {
  document.body.style.backgroundColor = `rgb(${red}, ${green}, ${blue})`;
  switch (e.keyCode) {
    case 38:
      document.body.style.backgroundColor = `rgb(${
        red < 255 ? (red += 10) : red
      }, ${green < 255 ? (green += 10) : green}, ${
        blue < 255 ? (blue += 10) : blue
      })`;
      upButton.style.color = `rgb(${red1 > 0 ? (red1 -= 10) : red1}, ${
        green1 > 0 ? (green1 -= 10) : green1
      }, ${blue1 > 0 ? (blue1 -= 10) : blue1})`;
      downButton.style.color = `rgb(${red < 255 ? (red += 10) : red}, ${
        green < 255 ? (green += 10) : green
      }, ${blue < 255 ? (blue += 10) : blue})`;

      break;
    case 40:
      document.body.style.backgroundColor = `rgb(${
        red > 0 ? (red -= 10) : red
      }, ${green > 0 ? (green -= 10) : green}, ${
        blue > 0 ? (blue -= 10) : blue
      })`;
      downButton.style.color = `rgb(${red2 < 255 ? (red2 += 10) : red2}, ${
        green2 < 255 ? (green2 += 10) : green2
      }, ${blue2 < 255 ? (blue2 += 10) : blue2})`;
      upButton.style.color = `rgb(${red > 0 ? (red -= 10) : red}, ${
        green > 0 ? (green -= 10) : green
      }, ${blue > 0 ? (blue -= 10) : blue})`;
      break;
  }
};

window.addEventListener("keydown", changeColor);
