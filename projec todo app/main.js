const toDoList = [];
const form = document.querySelector("form");
const ul = document.querySelector("ul");
const taskNumber = document.querySelector("h2 span");
let listItems = document.getElementsByClassName("task");
const firstInput = document.querySelector("#firstInput");
const secondInput = document.querySelector("#secondInput");
const btn = document.querySelector("button");

const removeTask = e => {
  taskNumber.textContent = listItems.length - 1;
  const index = e.target.parentNode.dataset.key;
  console.log(index);
  toDoList.splice(index, 1);
  renderList();
};

const addTask = e => {
  //

  e.preventDefault();
  const titleTask = firstInput.value;

  if (titleTask === "") return;

  const task = document.createElement("li");
  task.className = "task";
  task.innerHTML = "<button>-</button>" + titleTask;
  toDoList.push(task);
  renderList();
  ul.appendChild(task);
  firstInput.value = "";
  taskNumber.textContent = listItems.length;
  task.querySelector("button").addEventListener("click", removeTask);
};

const renderList = () => {
  ul.textContent = "";
  toDoList.forEach((toDoElement, key) => {
    toDoElement.dataset.key = key;
    ul.appendChild(toDoElement);
  });
};

const searchTask = e => {
  ul.textContent = "";
  const searchText = e.target.value.toLowerCase();
  let tasks = toDoList;
  tasks = tasks.filter(li => li.textContent.toLowerCase().includes(searchText));
  tasks.forEach(li => ul.appendChild(li));
};

secondInput.addEventListener("input", searchTask);

form.addEventListener("submit", addTask);
